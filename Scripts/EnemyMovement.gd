extends CharacterBody2D
class_name Enemy

@onready var sprite = $Sprite2D
@onready var timer = $Timer
@onready var collisionArea = $Area2D
var movementDirection = self.position
var speed = 50


func _ready():
	sprite.set_flip_h(true)

func _process(delta):
	if GameManager.player:
		look_at(GameManager.player.position)
		if GameManager.player.position.y < position.y:
			sprite.set_flip_v(true)
		else:
			sprite.set_flip_v(false)
		
		if timer.is_stopped():
			movementDirection = (GameManager.player.position - self.position).normalized()
			timer.start()
		
		velocity = movementDirection * speed
		move_and_slide()
	
	var overlap = collisionArea.get_overlapping_bodies()
	if overlap:
		for possbilePlayer in overlap:
			if possbilePlayer.is_in_group("Player"):
				possbilePlayer.combat.recieveDamage()
				die()
		
func die():
	GameManager.gameWorld.monstersKilled += 1
	if GameManager.ui:
		GameManager.ui.changeMonsterKillCount(GameManager.gameWorld.monstersKilled)
	if GameManager.gameWorld:
		if GameManager.gameWorld.monstersKilled % 7 == 0:
			GameManager.gameWorld.turnOffRandomLight()
	queue_free()
		
