extends Node

var player : Player
var gameWorld  : GameWorld
var ui : UI

var endScore = 0

func reset():
	player = null
	gameWorld = null
	ui = null

func _unhandled_input(event):
	if Input.is_action_pressed("key_exit"):
		get_tree().quit()

func gameScene():
	get_tree().change_scene_to_file("res://Game.tscn")

func gameOverScene():
	get_tree().change_scene_to_file("res://game_over.tscn")
