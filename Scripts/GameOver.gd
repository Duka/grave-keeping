extends Node2D

@onready var scoreLabel = $CanvasLayer/MarginContainer/MarginContainer/VBoxContainer/Label
@onready var button = $CanvasLayer/MarginContainer/MarginContainer/VBoxContainer/MarginContainer/Button

func _ready():
	GameManager.reset()
	scoreLabel.set_text("Monsters killed: " + str(GameManager.endScore))


func _on_button_pressed():
	GameManager.gameScene()
