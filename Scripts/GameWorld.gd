extends Node2D
class_name GameWorld

@onready var light = $Lights

@onready var enemy = load("res://Enemy.tscn")
@onready var enemyContainer = $Enemies
var maxEnemyCount = 25
var isSpawning = true
var spawnPoints = [[-20, -20], [280, -20], [20, 260], [280, 260]]

var monstersKilled = 0
var lightsOff = 0

@onready var easyTimer = $easyTimer
@onready var mediumTimer = $mediumTimer
@onready var hardTimer = $hardTimer

var currentTimer = easyTimer

@onready var spawnSound = $EnemySpawn
@onready var lightSound = $LightsOff

var rng = RandomNumberGenerator.new()

func _ready():
	GameManager.gameWorld = self
	currentTimer = easyTimer

func _process(delta):
	if currentTimer.is_stopped() and isSpawning == true:
		spawnMonster()
		currentTimer.start()
	
func turnOffRandomLight():
	var lights = light.get_children()
	if lights:
		var randomLightIndex = rng.randi_range(0, lights.size() - 1)
		lights[randomLightIndex].queue_free()
		lightsOff += 1
		lightSound.play()
	if lightsOff == 3:
		print("Switching to medium timer!")
		currentTimer = mediumTimer
	elif lightsOff == 8:
		print("Switching to hard timer!")
		currentTimer = hardTimer

func spawnMonster():
	if enemyContainer.get_children().size() < maxEnemyCount:
		var currentEnemy = enemy.instantiate()
		enemyContainer.add_child(currentEnemy)
		currentEnemy.global_position.x = spawnPoints[rng.randi_range(0, 3)][0]
		currentEnemy.global_position.y = spawnPoints[rng.randi_range(0, 3)][1]
		print("Spawned monster")
		spawnSound.play()
	else:
		print("Too many enemies")
