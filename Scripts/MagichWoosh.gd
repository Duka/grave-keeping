extends CharacterBody2D

@onready var collisionArea = $Area2D
@onready var timer = $Timer
var movementVector = null
const speed = 250

func _ready():
	movementVector = Vector2(0,0)
	
func _process(delta):
	if timer.is_stopped():
		queue_free()
	velocity = movementVector * speed
	move_and_slide()
	
	var overlap = collisionArea.get_overlapping_bodies()
	if overlap:
		for possbileEnemy in overlap:
			if possbileEnemy.is_in_group("Enemy"):
				possbileEnemy.die()
				die()
	else:
		pass
func setMovement():
	look_at(get_global_mouse_position())
	movementVector = (get_global_mouse_position() - self.position).normalized()

func die():
	queue_free()
