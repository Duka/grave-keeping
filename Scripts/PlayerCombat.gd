extends Node2D

@onready var wandTip = $WandSprite/Tip
@onready var woosh = load("res://MagichWoosh.tscn")
@onready var timer = $Timer

@onready var wooshSound = $ShootingWoosh
@onready var damageSound = $TakingDamage

var health = 5

func recieveDamage():
	print("ouch")
	health -= 1
	damageSound.play()
	if GameManager.ui:
		GameManager.ui.changeLifeCount(health)
	if health <= 0:
		die()
		
func die():
	print("You died lmao")
	GameManager.player = null
	if GameManager.gameWorld:
		GameManager.gameWorld.isSpawning = false
	GameManager.endScore = GameManager.gameWorld.monstersKilled
	GameManager.gameOverScene()

func _process(delta):
	look_at(get_global_mouse_position())

func _input(event):
	if event is InputEventMouseButton and timer.is_stopped():
		shoot()
		timer.start()
		
func shoot():
	var currentWoosh = woosh.instantiate()
	get_tree().root.add_child(currentWoosh)
	currentWoosh.global_position = wandTip.global_position
	currentWoosh.setMovement()
	wooshSound.play()
