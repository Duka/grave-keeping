extends CharacterBody2D
class_name Player

const Speed = 50.0
@onready var animationPlayer = $AnimationPlayer
@onready var sprite = $Sprite2D
@onready var wand = $Wand/WandSprite
@onready var combat = $Wand

func _ready():
	GameManager.player = self

func _physics_process(delta):

	var horizontalDirection = Input.get_axis("ui_left", "ui_right")
	var verticalDirection = Input.get_axis("ui_up", "ui_down")
	
	if horizontalDirection or verticalDirection:
		if !animationPlayer.is_playing():
			animationPlayer.play("Walk")
		velocity.x = horizontalDirection * Speed
		velocity.y = verticalDirection * Speed

	else:
		animationPlayer.play("Stand")
		velocity.x = move_toward(velocity.x, 0, Speed)
		velocity.y = move_toward(velocity.y, 0, Speed)

	move_and_slide()

func _input(event):
	var flipThreshhold = get_viewport().get_visible_rect().size[0] / 2
	if event is InputEventMouseMotion:
		if event.position.x > flipThreshhold:
			sprite.set_flip_h(true)
		else:
			sprite.set_flip_h(false)
