extends CanvasLayer
class_name UI

@onready var monstersKilled = $CanvasLayer/HBoxContainer/VBoxContainer/MarginContainer2/Label

@onready var life1 = $CanvasLayer/HBoxContainer/VBoxContainer/MarginContainer3/HBoxContainer/Life1
@onready var life2 = $CanvasLayer/HBoxContainer/VBoxContainer/MarginContainer3/HBoxContainer/Life2
@onready var life3 = $CanvasLayer/HBoxContainer/VBoxContainer/MarginContainer3/HBoxContainer/Life3
@onready var life4 = $CanvasLayer/HBoxContainer/VBoxContainer/MarginContainer3/HBoxContainer/Life4
@onready var life5 = $CanvasLayer/HBoxContainer/VBoxContainer/MarginContainer3/HBoxContainer/Life5

func _ready():
	GameManager.ui = self

func changeLifeCount(newLife):
	if newLife == 4:
		life5.hide()
	elif newLife == 3:
		life4.hide()
	elif newLife == 2:
		life3.hide()
	elif newLife == 1:
		life2.hide()
	elif newLife == 0:
		life1.hide()

func changeMonsterKillCount(killCount):
	monstersKilled.set_text("Monsters killed: " + str(killCount))
